<?php
/**
 * Created by PhpStorm.
 * User: pink
 * Date: 2019-07-09
 * Time: 12:51
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController {

    /**
     * @Route("")
     * @Template()
     */
    public function home() {
        $client = \Doctrine\CouchDB\CouchDBClient::create(array('dbname' => 'db1'));
        $allDocs = $client->allDocs();

      //  dd($allDocs);
    }



    protected function getSensorsConfiguration() {
        $sensors = array();

        for ($i=0;$i<=10;$i++) {
if ($i!=2 && $i!=10) continue;
            $sensors[$i] = array(
                'name' => "Senseur ".$i,
		'key' => $i
            );
        }

        return $sensors;
    }
    /**
     * @Route("sensors")
     * @Template()
     */
    public function sensors() {

        return array(
            'sensors' => $this->getSensorsConfiguration()
        );
    }

    /**
     * @Route("/data")
     * @Template()
     */
    public function data(Request $request) {

        $i = $request->get('sensor');
        $res = [] ;
        $res[] = array('t'=>date('Y-m-d H:i:s'),'v'=>rand(0,100)*$i,'sensor_id'=>$i);


        return $this->json($res);

        //  dd($allDocs);
    }

    /**
     * @Route("data2")
     * @Template()
     */
    public function data2(Request $request) {

	$client = \Doctrine\CouchDB\MangoClient::create(array('dbname' => 'db1'));
       // $docs = $client->allDocs();

$ts = time() - 2;
$date = date('Y-m-d\TH:i:s+02:00',$ts);
//dump($date);
$selector = ['timestamp'=>['$gt'=> $date]];
$options = ['limit'=>1,'skip'=>1,'use_index'=>['_design/doc','index'],'sort'=>[['_id'=>'desc']]];
$options=[];
$query = new \Doctrine\CouchDB\Mango\MangoQuery($selector,$options);
$docs = $client->find($query);




//dd($docs->body);
$res = [] ;

	foreach($docs->body['docs'] as $row) {
		
		
		
		$doc = $row;

		
		 $res[] = array(
                	't'=>$doc['d'],
                	'v'=>$doc['v'],
               		 'sensor_id'=>substr($doc['s'],1)
			);


	}

        
       



        return $this->json($res);

        //  dd($allDocs);
    }
}